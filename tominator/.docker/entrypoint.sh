#!/usr/bin/env sh
set -e pipefail

env

npm run $NODE_ENV

exec "$@"
