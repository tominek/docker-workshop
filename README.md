# Docker workshop

This workshop covers basics of using Docker and docker compose for both local development and production.

Workshop is based on article [Stop using Docker like a BFU](https://blog.devgenius.io/stop-using-docker-like-a-bfu-229215e6e06d).

