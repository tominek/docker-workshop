# Docker workshop cheatsheet

## Docker

### Commands
Full documentation [here](https://docs.docker.com/engine/reference/run/#docker-run-reference).

`docker ps -a` - show all containers (both running and stopped)

`docker images` - show all Docker images

`docker run` - run existing Docker image

`docker build` - build Docker image from Dockerfile

`docker exec` - execute command inside running Docker container

`docker logs` - show logs of running Docker container

`docker pull` - pull Docker image from registry

`docker rm` - remove

### Dockerfile
Full documentation [here](https://docs.docker.com/engine/reference/builder/).


`FROM` - defines parent docker image

`WORKDIR` - defines directory where following commands are executed

`COPY` - copy files or directories from local system inside the image

`RUN` - execute any command inside the image (npm install, etc.)

`ENTRYPOINT` - defines script/command which is executed on container start

`CMD` - defines command which is executed on container start but can be overridden by `ENTRYPOINT` or another `CMD`

`EXPOSE` - defines default exposed port on which container is listening

## Docker compose

### Configuration
Full documentation [here](https://docs.docker.com/compose/compose-file/compose-file-v3/).

`image` - defines name of used Docker image

`build` - can be used instead of `image` and specifies path to a Dockerfile (`context`) and optionally it's name (`dockerfile`)

`ports` - define port mapping between **localhost** and **container** (in this order)

`volumes` - define mounted directories which are shared between

`environment` - defines list of ENV variables that are provided to running container

### Commands
Full documentation [here](https://docs.docker.com/compose/reference/).

`docker-compose up (-d)` - creates or starts docker compose stack, in case there was change in **docker-compose.yml**, it recreates the stack

`docker-compose stop` - stops all containers in your current docker compose stack

`docker-compose down` - In addition to stopping all containers also deletes all of them together with all related networks, volumes and images

`docker-compose build (--no-cache) <SERVICE_NAME>` - Rebuilds all containers or only one specified by **SERVICE_NAME**

`docker-compose pull <SERVICE_NAME>` - Pulls all images of services with images defined or only one specified by **SERVICE_NAME**
